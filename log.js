const Reset = "\x1b[0m";
const Bright = "\x1b[1m";
const Dim = "\x1b[2m";
const Underscore = "\x1b[4m";
const Blink = "\x1b[5m";
const Reverse = "\x1b[7m";
const Hidden = "\x1b[8m";

const FgRed = "\x1b[31m";
const FgGreen = "\x1b[32m";
const FgBlue = "\x1b[34m";
const FgYellow = "\x1b[33m";

const BgRed = "\x1b[41m";
const BgGreen = "\x1b[42m";
const BgBlue = "\x1b[44m";
const BgYellow = "\x1b[43m";

const time = new Date().toISOString();

const log = {
    warning: (message) => console.warn(Bright, BgYellow, `[WARNING]`, Reset, Bright, `🤫 - ${time}`, message),
    error: (message) => console.error(Bright, BgRed, `[ERROR]`, Reset, Bright, `😖 - ${time}`, message),
    info: (message) => console.info(Bright, BgBlue, `[INFO]`, Reset, Bright, `😌 - ${time}`, message),
    success: (message) => console.info(Bright,  BgGreen, `[SUCCESS]`, Reset, Bright, `😎 - ${time}`, message),
    break: (message) => process.exit(console.error(Bright, BgRed, `[ERROR]`, Reset, Bright, `💩 - ${time}`, message))
};

export default log;
