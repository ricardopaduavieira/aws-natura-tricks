import { chromium } from "playwright";
import inquirer from 'inquirer';
import fetch from "node-fetch";
import fs from "fs";
import os from "os";
import dotenv from "dotenv";
import log from "./log.js";

dotenv.config();

const getUserRelatedInformation = () => {
    const user = process.env.USERID ?? null;
    const password = process.env.PASSWORD ?? null;
    const projectId = process.env.PROJECT_ID ?? null;
    const clientId = process.env.CLIENT_ID ?? null;
    const awsOutput = process.env.AWS_CREDENTIAL_OUTPUT ?? null;
    const awsRegionNonProd = process.env.AWS_CREDENTIAL_REGION_NON_PROD ?? null;
    const awsRegionProd = process.env.AWS_CREDENTIAL_REGION_PROD ?? null;
    const awsPath = `${os.homedir()}/.aws/credentials`;

    if (!user) log.break('USERID NOT FOUND IN YOUR ENVIRONMENT!');
    if (!password) log.break('PASSWORD NOT FOUND IN YOUR ENVIRONMENT!');
    if (!projectId) log.break('PROJECTID NOT FOUND IN YOUR ENVIRONMENT!');
    if (!clientId) log.break('CLIENTID NOT FOUND IN YOUR ENVIRONMENT!');
    if (!awsOutput) log.break('AWS OUTPUT NOT FOUND IN YOUR ENVIRONMENT!');
    if (!awsRegionNonProd) log.break('AWS REGION NON PROD NOT FOUND IN YOUR ENVIRONMENT!');
    if (!awsRegionProd) log.break('AWS REGION PROD NOT FOUND IN YOUR ENVIRONMENT!');

    return { user, password, projectId, clientId, awsPath, awsOutput, awsRegion: null };
};

const getSelectedEnvironment = async ({ userInfo }) => {
    const { environment } = await inquirer
    .prompt([
      {
        type: 'list',
        name: 'environment',
        message: 'WHICH ENVIRONMENT DO YOU WANT TO USE?',
        choices: [ "DEV", "QA", "HML", "PRD" ]
      }
    ]);

    if (environment.toString() === "PRD") {
        userInfo.awsRegion = process.env.AWS_CREDENTIAL_REGION_PROD;
    } else {
        userInfo.awsRegion = process.env.AWS_CREDENTIAL_REGION_NON_PROD;
    }

    return environment.toString().toLowerCase();
};

const getAccessToken = async ({ userInfo }) => {
    try {
        const browser = await chromium.launch({ headless: true });
        const page = await browser.newPage();

        await page.goto(`${process.env.URL_AUTH}`);
        await page.type("#codCN", userInfo.user);
        await page.type("#password", userInfo.password);
        await page.click("button[type=submit]");
        await page.waitForTimeout(5000);
        const accessToken = await page.evaluate(() => window.sessionStorage.getItem("@devops-fe/accessToken"));
        await browser.close();
    
        if (!accessToken) log.break(`ACCESSTOKEN NOT FOUND IN SESSIONSTORAGE ${process.env.URL_AUTH}`);
        delete userInfo.password;
        return accessToken;
    } catch (error) {

    }
};

const getRolesFromVault = async ({ userInfo }) => {
    try {
        const config = {
            headers: {
                "accept": "application/json",
                "accept-language": "en-US,en;q=0.9,pt-BR;q=0.8,pt;q=0.7,es;q=0.6",
                "content-type": "application/json;charset=UTF-8",
                "sec-ch-ua":
                '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
                "sec-ch-ua-mobile": "?0",
                "sec-fetch-dest": "empty",
                "sec-fetch-mode": "cors",
                "sec-fetch-site": "same-site",
                "access_token": userInfo.accessToken,
                "client_id": userInfo.clientId,
                "project_id": userInfo.projectId,
                "user_id": userInfo.user,
                "environment": userInfo.environment
              },
            referrer: `${process.env.URL_REFERRER}`,
            referrerPolicy: "strict-origin-when-cross-origin",
            method: "GET",
            mode: "cors",
          }
        const url = `${process.env.URL_VAULT}`;  
        const res = await fetch(url, config);
        const data = await res.json();
        if (!data?.length) log.break(`ROLES NOT FOUND FOR USERID ${userInfo.user} FROM VAULT`);
        return data;
    } catch (error) {

    }
};

const getSelectedRole = async ({ userInfo }) => {
    const { environment } = await inquirer
    .prompt([
      {
        type: 'list',
        name: 'environment',
        message: 'WHICH ROLE DO YOU WANT TO USE?',
        choices: userInfo.roles
      }
    ]);

    return environment.toString().toLowerCase();
};

const generateCredential = async ({ userInfo }) => {
    try {
        const config = {
            headers: {
                "accept": "application/json",
                "accept-language": "en-US,en;q=0.9,pt-BR;q=0.8,pt;q=0.7,es;q=0.6",
                "content-type": "application/json;charset=UTF-8",
                "sec-ch-ua":
                '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
                "sec-ch-ua-mobile": "?0",
                "sec-fetch-dest": "empty",
                "sec-fetch-mode": "cors",
                "sec-fetch-site": "same-site",
                "access_token": userInfo.accessToken,
                "client_id": userInfo.clientId,
                "project_id": userInfo.projectId,
                "user_id": userInfo.user,
                "environment": userInfo.environment,
                "role": userInfo.selectedRole
              },
            referrer: `${process.env.URL_REFERRER}`,
            referrerPolicy: "strict-origin-when-cross-origin",
            body: '{"reason":"cred"}',
            method: "POST",
            mode: "cors",
          }
        const url = `${process.env.URL_VAULT}`;  
        const res = await fetch(url, config);
        const data = await res.json();

        if (!data?.access_key || !data?.secret_key || !data?.security_token) {
            return log.break(`CREDENTIAL NOT GENERATED FOR USERID: ${userInfo.user}!`);
        }

        
        let _output = userInfo.awsOutput;
        let _region = userInfo.awsRegion;
        delete userInfo.awsOutput;
        delete userInfo.awsRegion;

        return {
            output: _output,
            region: _region,
            accessKey: data.access_key,
            secretKey: data.secret_key,
            securityToken: data.security_token
        }
    } catch (error) {

    }
};

const createCredentialFileInAwsPath = ({ userInfo }) => {
    userInfo.fileContent = `[default]
    output = ${userInfo.credential.output}
    region = ${userInfo.credential.region}
    aws_access_key_id = ${userInfo.credential.accessKey}
    aws_secret_access_key = ${userInfo.credential.secretKey}
    aws_session_token = ${userInfo.credential.securityToken}
    aws_security_token = ${userInfo.credential.securityToken}
    `;

    if(fs.existsSync(userInfo.awsPath)) {
        fs.unlinkSync(userInfo.awsPath);
    } 
    
    fs.writeFileSync(userInfo.awsPath, userInfo.fileContent);
    fs.chmodSync(userInfo.awsPath, '775');

    if(!fs.existsSync(userInfo.awsPath)) log.break('YOUR CREDENTIAL HAS NOT BEEN UPDATED!');

    return true;
};

(async (userInfo = {}) => {
    userInfo = getUserRelatedInformation();
    userInfo.environment = await getSelectedEnvironment({ userInfo });
    userInfo.accessToken = await getAccessToken({ userInfo });
    userInfo.roles = await getRolesFromVault({ userInfo });
    userInfo.selectedRole = await getSelectedRole({ userInfo });
    userInfo.credential = await generateCredential({ userInfo });
    userInfo.credentialUpdated =  createCredentialFileInAwsPath({ userInfo });
    log.success("YOUR CREDENTIAL WAS UPDATED!");

})();